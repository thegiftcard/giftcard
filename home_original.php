<?php
session_start();
require 'includes/db.php';
require 'includes/includes.php';
$titlename = " Index";
require 'includes/default_home_head.php';

?>

	  <div class="main">
	    <div class="wrap">
	       <div class="column_left">
	    		 <div class="menu_box">
	    		 	 <h3>Menu Box</h3>
	    		 	   <div class="menu_box_list">
				      		<ul>
						  		<li><a href="#" class="messages"><span>Messages</span><label class="digits">24</label><div class="clear"></div></a></li>
						  		<li class="active"><a href="#" class="invites"><span>Invites</span><label class="digits active">3</label><div class="clear"></div></a></li>
						  		<li><a href="#" class="events"><span>Events</span><label class="digits">5</label><div class="clear"></div></a></li>
						  		<li><a href="#" class="account_settings"><span>Account Settings</span></a></li>
						  		<li><a href="#" class="statistics"><span>Statistics</span></a></li>
				    		</ul>
				      </div>
	    		 </div>


	    		 <div class="chart">
		               <h3>Os Audience Stats</h3>
		                <div class="diagram">
		                  <canvas id="canvas" height="220" width="220"> </canvas>
		                  <h4><span>June</span><br />2013</h4>
		                 </div>

		               <!----
						<span><img src="layout_v1/images/chart.png" alt="" /></span>
						----->
						<div class="chart_list">
			           	  <ul>
			           	  	<li><a href="#" class="red">ios<p class="percentage">21<em>%</em></p></a></li>
			           	  	<li><a href="#" class="purple">Mac<p class="percentage">48<em>%</em></p></a></li>
			           	  	<li><a href="#" class="yellow">Linux<p class="percentage">9<em>%</em></p></a></li>
			           	  	<li><a href="#" class="blue">Win<p class="percentage">32<em>%</em></p></a></li>
			           	  	<div class="clear"></div>
			           	  </ul>
			           </div>
			           <script>
						var doughnutData = [
								{
									value: 21,
									color:"#E64C65"
								},
								{
									value : 47,
									color : "#11A8AB"
								},
								{
									value : 32,
									color : "#4FC4F6"
								},
								{
									value : 9,
									color : "#FCB150"
								},

							];
							var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut(doughnutData);
					</script>
		          </div>
		             <div class="graph">
		             	<canvas id="line-chart"> </canvas>
		             	<script>
							var lineChartData = {
								labels : ["Apr","May","Jun","JUl","Aug","Sep"],
								datasets : [
									{
										fillColor : "rgba(255, 255, 255, 0)",
										strokeColor : "#FFF",
										pointColor : "#11a8ab",
										pointStrokeColor : "#FFF",
										data : [21,48,35,21,48,35]
									}
								]

							}

						var myLine = new Chart(document.getElementById("line-chart").getContext("2d")).Line(lineChartData);

						</script>
		             	<!---
		             	<img src="layout_v1/images/graph.png" alt="" />
		             	------->
		             	<div class="graph_list">
		             		 <div class="week-month-year">
		             		 	<p><a href="#">Week</a></p>
		             		 	<p><a href="#" class="active">Month</a></p>
		             		 	<p><a href="#">year</a></p>
		             		 	<div class="clear"></div>
		             		 </div>
				      			<ul>
						  		    <li><a href="#"><span class="day_name">Apr</span> 2013
						  			<label class="digits"><span>+</span>21<em>%</em></label><div class="clear"></div></a></li>
						  			<li class="active"><a href="#"><span class="day_name">May</span> 2013
						  			<label class="digits"><span>+</span>48<em>%</em></label><div class="clear"></div></a></li>
						  			<li><a href="#"><span class="day_name">Jun</span> 2013
						  			<label class="digits"><span>+</span>35<em>%</em> </label><div class="clear"></div></a></li>
				    		</ul>
				      </div>
		             </div>
		                 <div class="video_palyer">
								<iframe src="//player.vimeo.com/video/24363983" width="100%" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
		                </div>

	  		</div>

            <div class="column_middle">
              <div class="column_middle_grid1">
		         <div class="profile_picture">
		        	<a href=""><img src="layout_v1/images/profile_img150x150.jpg" alt="" />	</a>
		            <div class="profile_picture_name">
		            	<h2>Anne Hathaway</h2>
		            	<p>Lorem ipsum dolor sit amet consectetur adipisicing </p>
		            </div>
		             <span><a href="#"> <img src="layout_v1/images/follow_user.png" alt="" /> </a></span>
		          </div>
		           <div class="articles_list">
		           	  <ul>
		           	  	<li><a href="#" class="red"> <img src="layout_v1/images/comments.png" alt="" /> 23</a></li>
		           	  	<li><a href="#" class="purple"> <img src="layout_v1/images/views.png" alt="" /> 841</a></li>
		           	  	<li><a href="#" class="yellow"> <img src="layout_v1/images/likes.png" alt="" /> 49</a></li>
		           	  	<div class="clear"></div>
		           	  </ul>
		           </div>
		       </div>
		         <div class="weather">
		               <h3><i><img src="layout_v1/images/location.png" alt="" /> </i> Cluj-Napoca / Ro</h3>
		              <div class="today_temp">
		                <div class="temp">
							<figure>Fri 29/06<span>24<em>o</em></span></figure>
						</div>
						<img src="layout_v1/images/sun.png" alt="" />
					</div>
						<div class="temp_list">
				      			<ul>
						  		    <li><a href="#"><span class="day_name">Sat</span>&nbsp; 29/06
						  			<label class="digits">25<em>o</em> <i><img src="layout_v1/images/sun_icon1.png" alt="" /></i></label><div class="clear"></div></a></li>
						  			<li class="active"><a href="#"><span class="day_name">Sun</span>&nbsp; 30/06
						  			<label class="digits">22<em>o</em> <i><img src="layout_v1/images/clouds.png" alt="" /></i></label><div class="clear"></div></a></li>
						  			<li><a href="#"><span class="day_name">Mon</span>&nbsp; 01/07
						  			<label class="digits">24<em>o</em> <i><img src="layout_v1/images/clouds.png" alt="" /></i></label><div class="clear"></div></a></li>
						  			<li><a href="#"><span class="day_name">Tue</span>&nbsp; 02/07
						  			<label class="digits">26<em>o</em> <i><img src="layout_v1/images/sun_icon1.png" alt="" /></i></label><div class="clear"></div></a></li>
						  			<li><a href="#"><span class="day_name">Wed</span>&nbsp; 03/07
						  			<label class="digits">27<em>o</em> <i><img src="layout_v1/images/sun_icon2.png" alt="" /></i></label><div class="clear"></div></a></li>
						  			<li><a href="#"><span class="day_name">Thu</span>&nbsp; 04/07
						  			<label class="digits">29<em>o</em> <i><img src="layout_v1/images/sun_icon2.png" alt="" /></i></label><div class="clear"></div></a></li>
				    		</ul>
				      </div>
		          </div>
    	    </div>

            <div class="column_right">
            	<div class="column_right_grid">
                 <div class="newsletter">
				   <h3>CSM Volleybal Schema</h3>
<iframe style="border-width: 0;" src="https://www.google.com/calendar/embed?title=CSM%20Volleybal&amp;showTitle=0&amp;showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showTz=0&amp;mode=AGENDA&amp;height=500&amp;wkst=2&amp;bgcolor=%234C4B4B&amp;src=44mpfn54fge9vjlsndlp7104hk%40group.calendar.google.com&amp;color=%23B1440E&amp;src=p0sghrup9psgpm0o1uirv4i2i0%40group.calendar.google.com&amp;color=%235229A3&amp;src=1m4p9l9gc0oepbku3lbolsbb7c%40group.calendar.google.com&amp;color=%23875509&amp;ctz=Europe%2FAmsterdam" name="calendar" height="500" frameborder="0" scrolling="no"></iframe>
				   </div>
           </div>

           <div class="column_right_grid sign-in">
           <div class="newsletter">
    <h3>CC</h3>
    <script src="https://widgets.coingecko.com/coingecko-coin-list-widget.js"></script>
<coingecko-coin-list-widget  coin-ids="ethereum,siacoin,basic-attention-token,mobilego,wagerr,nxt,status,tenx,neo,agrello,aeron,chainlink,cloakcoin,district0x,dogecoin,lisk,chainlink,ignis,gas,iota,augur,wepower,binancecoin,te-food,friendz,ontology,farmatrust,face,cashaa,bitcoin,enjincoin,concierge-io,monaco,theta-token" background-color="A9A9A9" currency="eur" locale="en"></coingecko-coin-list-widget>

    </div>
			   </div>

				 <div class="column_right_grid sign-in">
				 	<div class="sign_in">
				       <h3>Sign in to your account</h3>
					    <form>
					    	<span>
					 	    <i><img src="layout_v1/images/mail.png" alt="" /></i><input type="text" value="Enter your email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email';}">
					 	    </span>
					 	    <span>
					 	     <i><img src="layout_v1/images/lock.png" alt="" /></i>
					 	     <input type="password" placeholder="password">
					 	    </span>
					 		<input type="submit" class="my-button" value="Sign In">
					 	</form>
					       <h4><a href="#">Forget Password?</a></h4>
          		       </div>
				   </div>
				   <div class="column_right_grid downloading_uploading">
                      <ul>
					      <li>
                            <div class="progress">
                              <div class="progress-bar" style="width:81%"> </div>
                            </div>
                            <div class="clearfix">
                                <p class="downloading"> <i><img src="layout_v1/images/download.png" alt="" /></i> Downloading...</p>
                                <p class="percentage">81<em>%</em></p>
                                <div class="clear"></div>
                            </div>
                           </li>
                           <li>
                            <div class="progress">
                              <div class="progress-bar uploading" style="width:43%"> </div>
                            </div>
                            <div class="clearfix">
                                <p class="downloading"><i><img src="layout_v1/images/upload.png" alt="" /></i> Uploading...</p>
                                <p class="percentage">43<em>%</em></p>
                                <div class="clear"></div>
                            </div>
                           </li>
                        </ul>
				   </div>
				   <div class="column_right_grid date_events">
				     	  <h3><a href="#" id="slide_prev"><img src="layout_v1/images/arrow-left.png" alt="" /></a> Wednesday <a href="#" id="slide_next"><img src="layout_v1/images/arrow-right.png" alt="" /></a></h3>
				     	 		<ul id="slide" class="event_dates">
						            <li>26</li>
						            <li>25</li>
						            <li>24</li>
						            <li>23</li>
						            <li>22</li>
						            <li>21</li>
						            <li>20</li>
						            <li>19</li>
						            <li>18</li>
						            <li>17</li>
						            <li>16</li>
						            <li>15</li>
						            <li>14</li>
						            <li>13</li>
						            <li>12</li>
						            <li>11</li>
						            <li>10</li>
						            <li>9</li>
						            <li>8</li>
						            <li>7</li>
						            <li>6</li>
						            <li>5</li>
						            <li>4</li>
						            <li>3</li>
						            <li>2</li>
						            <li>1</li>
						            <li>31</li>
						            <li>30</li>
						            <li>29</li>
						            <li>28</li>
						            <li>27</li>
						        </ul>
						         <div class="button"><a href="#">Add Event</a></div>
				           <script type="text/javascript">
				            $(function() {
				                $('#slide').ulslide({

									effect: {
										type: 'carousel', // slide or fade
				                        axis: 'x',        // x, y
										showCount:1
									},
				                    nextButton: '#slide_next',
				                    prevButton: '#slide_prev',
				                    duration: 800,
				                    /*autoslide: 2000,*/
									easing: 'easeOutBounce'
				                });
				            });
				        </script>
				    </div>
				   <div class="column_right_grid calender">
                      <div class="cal1"> </div>
				   </div>
             </div>
    	<div class="clear"></div>
 	 </div>
   </div>
  		 <div class="copy-right">
				<p>© 2013 Designed by <a href="http://graphicburger.com/flat-design-ui-components/" target="_blank">GraphicBurger</a>  • Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a> </p>
	 	 </div>
</body>
</html>
