<?php
require '../includes/db.php';

// db_calls.php is a php page with multiple functions, they are called with the CALL _get

// Explains:
// json_totalen = shows the total value in a json format.
// db_totalen = Calculating the totals from the DB.
//
//

if (isset($_GET['call']))
{ //user logging in
  $my_string = filter_input(INPUT_GET, 'call', FILTER_SANITIZE_STRING);
  $my_string_ha = filter_input(INPUT_GET, 'ha', FILTER_SANITIZE_STRING);
  //echo $my_string;
  //echo "<BR>";

  if ($my_string == "TEMPLATE")
  {
    //
  }

    if ($my_string == "db_totalen")
    {
        $sql31 = "SELECT * FROM crypto_totalen WHERE `markt` = 'totalen'";
        if($result31 = mysqli_query($link_db_crypto_giftcard, $sql31))
        {
          while($row31 = mysqli_fetch_array($result31))
          {
          $counttotaalwaarde = $row31['totaalwaarde'];

          }
        }


        $sql33 = "SELECT * FROM crypto_coingecko where name = 'bitcoin'";
        if($result33 = mysqli_query($link_db_crypto_giftcard, $sql33))
        {

          while($row33 = mysqli_fetch_array($result33))
          {
            $btc_calc = $row33['current_price'];
            $btc_calc = substr($btc_calc, 0, -5);
          }
        }
        $btc_calc = $counttotaalwaarde / $btc_calc;
        $btc_calc = substr($btc_calc, 0, -10);

        $sql333 = "SELECT * FROM crypto_coingecko where name = 'ethereum'";
        if($result333 = mysqli_query($link_db_crypto_giftcard, $sql333))
        {

          while($row333 = mysqli_fetch_array($result333))
          {
            $eth_calc = $row333['current_price'];
            $eth_calc = substr($eth_calc, 0, -5);
          }
        }
        $eth_calc = $counttotaalwaarde / $eth_calc;
        $eth_calc = substr($eth_calc, 0, -10);

        $decoded_json = json_decode(file_get_contents("../../../datafiles/eur_to_usd.json"), TRUE);
        $eur_to_usd = $decoded_json['rates']['USD'];

        $counttotaalwaarde_usd = $counttotaalwaarde * $eur_to_usd;

        $counttotaalwaarde_usd = substr($counttotaalwaarde_usd, 0, -4);
        $counttotaalwaarde = substr($counttotaalwaarde, 0, -4);
        if ($my_string_ha == "yes")
        {
          echo "<div class=total-value-eur>\r\n<h1>\r\nTotal Value: &euro;".$counttotaalwaarde."\r\n</h1>\r\n</div>";
          echo "<div class=total-value-usd>\r\n<h1>\r\nTotal Value: &dollar;".$counttotaalwaarde_usd."\r\n</h1>\r\n</div>";
          echo "<div class=total-value-btc>\r\n<h1>\r\nTotal Value: &#8383;".$btc_calc."\r\n</h1>\r\n</div>";
          echo "<div class=total-value-eth>\r\n<h1>\r\nTotal Value: &#926;".$eth_calc."\r\n</h1>\r\n</div>";
          exit;
        }
        echo "&dollar; ".$counttotaalwaarde_usd." - &euro; ".$counttotaalwaarde."<BR> &#8383; ".$btc_calc." - &#926; ".$eth_calc;
    }


      if ($my_string == "db_totalen_OLD_OLD_OLD")
      {
          $sql31 = "SELECT * FROM crypto_totalen WHERE `markt` != 'totalen'";
          if($result31 = mysqli_query($link_db_crypto_giftcard, $sql31))
          {
            while($row31 = mysqli_fetch_array($result31))
            {
              $id = $row31['id'];
              $sql3 = "SELECT * FROM crypto_totalen where id = '$id'";
              if($result3 = mysqli_query($link_db_crypto_giftcard, $sql3))
              {

                while($row3 = mysqli_fetch_array($result3))
                {

                  $markt = $row3['markt'];
                  $maxwaarde = $row3['maxwaarde'];
                  $totaalwaarde = $row3['totaalwaarde'];
                  //echo $markt." - ";
                  //echo " &euro; ".$maxwaarde." - ";
                  //echo " &euro; ".$totaalwaarde."";
                  //echo "<BR>";
                  $counttotaalwaarde = $counttotaalwaarde + $totaalwaarde;
                  $countmaxwaarde = $countmaxwaarde + $maxwaarde;
                }
              }
            }
          }

          $sql33 = "SELECT * FROM crypto_coingecko where name = 'bitcoin'";
          if($result33 = mysqli_query($link_db_crypto_giftcard, $sql33))
          {

            while($row33 = mysqli_fetch_array($result33))
            {
              $btc_calc = $row33['current_price'];
              $btc_calc = substr($btc_calc, 0, -5);
            }
          }
          $btc_calc = $counttotaalwaarde / $btc_calc;
          $btc_calc = substr($btc_calc, 0, -10);

          $sql333 = "SELECT * FROM crypto_coingecko where name = 'ethereum'";
          if($result333 = mysqli_query($link_db_crypto_giftcard, $sql333))
          {

            while($row333 = mysqli_fetch_array($result333))
            {
              $eth_calc = $row333['current_price'];
              $eth_calc = substr($eth_calc, 0, -5);
            }
          }
          $eth_calc = $counttotaalwaarde / $eth_calc;
          $eth_calc = substr($eth_calc, 0, -10);


          $counttotaalwaarde = substr($counttotaalwaarde, 0, -4);
          echo "&euro; ".$counttotaalwaarde." - &#8383; ".$btc_calc." - &#926; ".$eth_calc;
      }












  if ($my_string == "json_totalen")
  {
        $sql = "SELECT totaalwaarde FROM `crypto_totalen` WHERE `markt` = 'totalen'";
        $queryRecords = mysqli_query($link_db_crypto_giftcard, $sql) or die("error to fetch _totalen data");

        //$sth = mysqli_query("SELECT ...");
        $rows = array();
        while($r = mysqli_fetch_assoc($queryRecords))
        {
          $rows[] = $r;
        }
      print json_encode($rows);
      }

} else {
  echo "Error code maximus giganticus..";
  // Break is "CALL" was not recognized
}

?>
