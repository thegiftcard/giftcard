<?php

require '../../includes/db.php';


$sql = "DELETE FROM crypto WHERE `reg_date` < CURRENT_TIMESTAMP - INTERVAL 1 MONTH";

echo "####################################<BR>";
echo "#Cleanup task 01:<BR>";
echo "## - Remove all rows older then 1 months from table CRYPTO.<BR>";
echo "## - Run sql:".$sql."<BR> ## - Starting command, status report:<BR>";
//echo "placeholder for housekeeper to clean up the DB";


if ($link_db_crypto_giftcard->query($sql) === TRUE) {
  echo "## - Record deleted successfully";
} else {
  echo "## - Error deleting record: " . $link_db_crvypto_giftcard->error;
}
$link_db_crypto_giftcard->close();
echo "<BR><BR><BR>####################################<BR>";
echo "<BR>";
echo "####################################<BR>";
echo "<BR>Cleanup Task 02:<BR>";
?>
