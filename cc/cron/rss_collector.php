<?php

require '../../includes/db.php';
date_default_timezone_set('UTC'); // Potential for mistakes
$sql = "SELECT * FROM rss_feeds";
if($result = mysqli_query($link_db_crypto_giftcard, $sql))
{
  $MultiDimArray = array();
  while($row = mysqli_fetch_array($result))
  {

    $combo = $row['Subject']." ".$row['Source'];
    //echo $combo;
     $MultiDimArray[] = array ( 'name' => "$combo", 'url' => $row['URL'] );

  }
}
//print_r($MultiDimArray);
//echo "<BR><BR>";

$rss = new DOMDocument();
$feed = array();
$urlarray = array(
  array( 'name' => 'TE-FOOD Medium',          'url' => 'https://medium.com/feed/@te_food' ),
  array( 'name' => 'TE-FOOD Twitter',         'url' => 'https://twitrss.me/twitter_user_to_rss/?user=te_food' ),
  array( 'name' => 'TE-FOOD Reddit',          'url' => 'https://www.reddit.com/r/TE-FOOD/' ),
  array( 'name' => 'Chain.link Blog',         'url' => 'https://blog.chain.link/rss/' ),
  array( 'name' => 'Chain.link Twitter',      'url' => 'https://twitrss.me/twitter_user_to_rss/?user=chainlink' ),
  array( 'name' => 'Smartcontract Twitter',   'url' => 'https://twitrss.me/twitter_user_to_rss/?user=smart_contract' ),
  array( 'name' => 'Chainlink Reddit',        'url' => 'https://www.reddit.com/r/Chainlink/' ),
  array( 'name' => 'Sergey Nazarov Twitter',  'url' => 'https://twitrss.me/twitter_user_to_rss/?user=sergeynazarov' ),
//  array( 'name' => '',   'url' => '' ),
);

foreach ( $MultiDimArray as $url ) {
  $rss->load( $url['url'] );

  foreach ( $rss->getElementsByTagName( 'item' ) as $node ) {
  $item = array(
    'site'  => $url['name'],
    'title' => $node->getElementsByTagName( 'title' )->item( 0 )->nodeValue,
    'desc'  => $node->getElementsByTagName( 'description' )->item( 0 )->nodeValue,
    'link'  => $node->getElementsByTagName( 'link' )->item( 0 )->nodeValue,
    'date'  => $node->getElementsByTagName( 'pubDate' )->item( 0 )->nodeValue,
  );

  array_push( $feed, $item );
  }
}

usort( $feed, function( $a, $b ) {
  return strtotime( $b['date'] ) - strtotime( $a['date'] );
});

$limit = 30;
echo "Edit these lines to add the feed to DB";
echo '<ul>';
for ( $x = 0; $x < $limit; $x++ ) {
    $site = $feed[ $x ]['site'];
    $title = str_replace( ' & ', ' & ', $feed[ $x ]['title'] );
    $titlecount = strlen($title);
    $titlelength = "80";
    $titlelength = $titlecount - $titlelength;
    $title_short = substr($title, 0, -$titlelength);  // returns "abcde"
  //  $title_short = str_replace( ' & ', ' & ', $feed[ $x ]['title'] );

    $link = $feed[ $x ]['link'];
    $description = $feed[ $x ]['desc'];
    $date = date( 'Ymd', strtotime( $feed[ $x ]['date'] ) );

    echo '<li>';
    echo ''.$date.' - <strong>'.$site.': <a href="'.$link.'" title="'.$title.'" target="_blank">'.$title_short.'...</a></strong> ';
    echo '</li>';
    $site = explode(" ", $site);
    //echo $site[0]; // piece1
    //echo $site[1]; // piece2
    $hash_content = $site."_".$title_short;
    $hash = hash('md5', $hash_content);



    $query3 = "SELECT `hash` FROM `rss_feeds_content` WHERE `hash` = '".$hash."'";
    $result = mysqli_query($link_db_crypto_giftcard,$query3);

    if(mysqli_num_rows($result) > 0)
    {
      echo "<BR>Row does exist: $hash<BR>";
    } else {
      $title = str_replace("'", "\'", $title);
      $sql = "insert into rss_feeds_content(`id`,`Read`,`Source`,`Subject`,`URL`,`Title`,`Clicks`,`hash`,`article_date`,`reg_date`) values('','0','$site[1]','$site[0]','$link','$title','0','$hash', '$date', CURRENT_TIMESTAMP)";
      echo $sql;
      mysqli_query($link_db_crypto_giftcard,$sql);
    }

}
echo '</ul>';
mysqli_close($link_db_crypto_giftcard);
?>
