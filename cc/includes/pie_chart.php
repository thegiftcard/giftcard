<?php
require '../../includes/db.php';


if (isset($_GET['call']))
{ //user logging in
  $my_string = filter_input(INPUT_GET, 'call', FILTER_SANITIZE_STRING);
  //echo $my_string;
  //echo "<BR>";

  if ($my_string == "TEMPLATE")
  {
    //
  }

  if ($my_string == "piechart")
  {
    echo <<<EOF
      <div class="chart">
        <h3>Crypto spread<BR>Exchanges</h3>
        <div class="diagram">
          <canvas id="canvas" height="220" width="220"> </canvas>
          Exchange lower that €30,- are not shown.
        </div>
        <!----
          <span><img src="layout_v1/images/chart.png" alt="" /></span>
        ----->
        <div class="chart_list">
          <ul>
EOF;

          $colors=array(
            'e64c65','11a8ab','fcb150','008000',
          );//colors array

          $count = 0;
          $sql31 = "SELECT * FROM crypto_totalen WHERE `markt` != 'totalen' AND `totaalwaarde` > '30' ORDER BY totaalwaarde DESC";
          if($result31 = mysqli_query($link_db_crypto_giftcard, $sql31))
          {
            while($row31 = mysqli_fetch_array($result31))
            {
              $markt = $row31['markt'];
              $id = $row31['id'];
              $totaalwaarde = $row31['totaalwaarde'];
              $totaalwaarde = substr($totaalwaarde, 0, -5);
              echo "<li><a href=\"home.php?markt=$id\" alt=\"Hello\" style=\"border-top:5px solid #$colors[$count]\">$markt<p class=\"percentage\">€ $totaalwaarde</p></a></li>\n";
              $count = $count + 1;
              if($count == "4") {
                $count = 0;
                echo "          <div class=\"clear\"></div></ul><ul>";
              }
            }
          }


      echo <<<EOF
          <div class="clear"></div></ul>
        </div>
        <script>
          var doughnutData = [
EOF;

        $count = 0;
        $sql31 = "SELECT * FROM crypto_totalen WHERE `markt` != 'totalen' AND `totaalwaarde` > '30' ORDER BY totaalwaarde DESC";
        if($result31 = mysqli_query($link_db_crypto_giftcard, $sql31))
        {
          while($row31 = mysqli_fetch_array($result31))
          {
            $markt = $row31['markt'];
            $totaalwaarde = $row31['totaalwaarde'];
            $totaalwaarde = substr($totaalwaarde, 0, -5);
            echo "{\n";
              echo "value : $totaalwaarde,\n";
              echo "color : \"#$colors[$count]\"\n";
              echo "},\n";
              $count = $count + 1;
              if($count == "4") {
                $count = 0;
              }
            }
          }

      echo <<<EOF

          ];
          var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut(doughnutData);
        </script>
      </div>
EOF;
  }


// Piechart2 is for the coinchart.
  if ($my_string == "piechart2")
  {
    echo <<<EOF
      <div class="chart">
        <h3>Crypto spread<BR>Coins</h3>
        <div class="diagram">
          <canvas id="canvas" height="220" width="220"> </canvas>
          Coins lower that €30,- are not shown.
        </div>
        <!----
          <span><img src="layout_v1/images/chart.png" alt="" /></span>
        ----->
        <div class="chart_list">
          <ul>
EOF;

          $colors=array(
            'e64c65','11a8ab','fcb150','008000',
          );//colors array

          $count = 0;
          $sql31 = "SELECT * FROM crypto_name WHERE `coin_name` != 'Totaal' AND `waarde` > '30' ORDER BY waarde DESC";
          if($result31 = mysqli_query($link_db_crypto_giftcard, $sql31))
          {
            while($row31 = mysqli_fetch_array($result31))
            {
              $coin_name = $row31['coin_name'];
              $markt = $row31['Markt'];
              $id = $row31['id'];
              //$aantal_coins = $row31['value'];
              $aantal_coins = substr($row31['value'], 0, -7);
              $totaalwaarde = $row31['waarde'];
              $totaalwaarde = substr($totaalwaarde, 0, -10);
              $sql319 = "SELECT * FROM crypto_coingecko WHERE `id` = '$coin_name'";
              if($result319 = mysqli_query($link_db_crypto_giftcard, $sql319))
              {
                while($row319 = mysqli_fetch_array($result319))
                {
                  $image = $row319['image'];
                }
              } else {
                $image = "non_exist";
              }
              echo "<li><a href=\"home.php?markt=$id\" alt=\"Hello\" style=\"border-top:5px solid #$colors[$count]\"><img src=\"$image\" width=\"40\" alt=\"$aantal_coins $coin_name - $markt\" title=\"$aantal_coins $coin_name - $markt\"><br> $markt<p class=\"percentage\">€ $totaalwaarde</p></a></li>\n";
              //echo "<li><a href=\"home.php?markt=$id\" alt=\"Hello\" style=\"border-top:5px solid #$colors[$count]\"><img src=\"$image\" width=\"40\" alt=\"$coin_name - $markt\" title=\"$coin_name - $markt\"> <B>$coin_name</b><br> $markt<p class=\"percentage\">€ $totaalwaarde</p></a></li>\n";
              $count = $count + 1;
              if($count == "4") {
                $count = 0;
                echo "          <div class=\"clear\"></div></ul><ul>";
              }
            }
          }


      echo <<<EOF
          <div class="clear"></div></ul>
        </div>
        <script>
          var doughnutData = [
EOF;

        $count = 0;
        $sql31 = "SELECT * FROM crypto_name WHERE `coin_name` != 'Totaal' AND `waarde` > '30' ORDER BY waarde DESC";
        if($result31 = mysqli_query($link_db_crypto_giftcard, $sql31))
        {
          while($row31 = mysqli_fetch_array($result31))
          {
            $markt = $row31['coin_name'];
            $totaalwaarde = $row31['waarde'];
            $totaalwaarde = substr($totaalwaarde, 0, -10);
            echo "{\n";
              echo "value : $totaalwaarde,\n";
              echo "color : \"#$colors[$count]\"\n";
              echo "},\n";
              $count = $count + 1;
              if($count == "4") {
                $count = 0;
              }
            }
          }

      echo <<<EOF

          ];
          var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut(doughnutData);
        </script>
      </div>
EOF;
  }




} else {
  echo "Error code maximus giganticus..";
  // Break is "CALL" was not recognized
}

?>
